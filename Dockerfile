FROM openjdk:11.0.16-oracle

WORKDIR /app

EXPOSE 9700

COPY target/*.jar notification.jar

ENTRYPOINT [ "java","-jar","./notification.jar"]
