package com.powrfit.notification;

import java.util.Map;

import lombok.ToString;

@lombok.Data
@ToString
public class notification {
    private String title;
    private String body;

}
