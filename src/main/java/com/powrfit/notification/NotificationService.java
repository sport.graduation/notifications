package com.powrfit.notification;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.Notification;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.annotation.RabbitListenerConfigurer;
import org.springframework.amqp.rabbit.listener.RabbitListenerEndpointRegistrar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NotificationService implements RabbitListenerConfigurer {

    @Autowired
    private FirebaseMessaging firebaseMessaging;

    @Override
    public void configureRabbitListeners(RabbitListenerEndpointRegistrar rabbitListenerEndpointRegistrar) {
    }

    @RabbitListener(queues = "${playerNotification.rabbitmq.queue}")
    public void sendNotification(notification directNotification, String token) throws FirebaseMessagingException {
        Notification notification = Notification
            .builder()
            .setTitle(directNotification.getTitle())
            .setBody(directNotification.getBody())
            .build();
        Message message = Message.builder().setToken(token).setNotification(notification).build();

        firebaseMessaging.send(message);
    }

    @RabbitListener(queues = "${teamNotification.rabbitmq.queue}")
    public void sendNotificationTopic(notification directNotification, String topic) throws FirebaseMessagingException {
        Notification notification = Notification
            .builder()
            .setTitle(directNotification.getTitle())
            .setBody(directNotification.getBody())
            .build();
        Message message = Message.builder().setNotification(notification).setTopic(topic).build();

        firebaseMessaging.send(message);
    }
}
