package com.powrfit.notification.Config;



import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {

    @Value("${playerNotification.rabbitmq.queue}")
    private String queuePlayerNotification;

    @Value("${playerNotification.rabbitmq.exchange}")
    private String exchangePlayerNotification;

    @Value("${playerNotification.rabbitmq.routingkey}")
    private String routingkeyPlayerNotification;

    @Value("${teamNotification.rabbitmq.queue}")
    private String queueTeamNotification;

    @Value("${teamNotification.rabbitmq.exchange}")
    private String exchangeTeamNotification;

    @Value("${teamNotification.rabbitmq.routingkey}")
    private String routingkeyTeamNotification;

    @Value("${spring.rabbitmq.username}")
    private String username;

    @Value("${spring.rabbitmq.password}")
    private String password;

    @Value("${spring.rabbitmq.host}")
    private String host;

    @Bean
    Queue queuePlayerNotification() {
        return new Queue(queuePlayerNotification, true);
    }

    @Bean
    Queue queueTeamNotification() {
        return new Queue(queueTeamNotification, true);
    }

    @Bean
    Exchange exchangePlayerNotification() {
        return ExchangeBuilder.directExchange(exchangePlayerNotification).durable(true).build();
    }

    @Bean
    Exchange exchangeTeamNotification() {
        return ExchangeBuilder.directExchange(exchangeTeamNotification).durable(true).build();
    }

    @Bean
    Binding bindingPracticeData() {
        return BindingBuilder
                .bind(queuePlayerNotification())
                .to(exchangePlayerNotification())
                .with(routingkeyPlayerNotification)
                .noargs();
    }
    @Bean
    Binding bindingDailyData() {
        return BindingBuilder
                .bind(queueTeamNotification())
                .to(exchangeTeamNotification())
                .with(routingkeyTeamNotification)
                .noargs();
    }

    @Bean
    public ConnectionFactory connectionFactory() {
        CachingConnectionFactory cachingConnectionFactory = new CachingConnectionFactory(host);
        cachingConnectionFactory.setUsername(username);
        cachingConnectionFactory.setPassword(password);
        return cachingConnectionFactory;
    }

    @Bean
    public MessageConverter jsonMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(jsonMessageConverter());
        return rabbitTemplate;
    }
}