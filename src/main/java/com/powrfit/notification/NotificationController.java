package com.powrfit.notification;

import com.google.firebase.messaging.FirebaseMessagingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/api/v1/notifications")
@RestController
public class NotificationController {

    @Autowired
    private NotificationService notificationService;

    @RequestMapping(value = "/send-notification-direct",method = RequestMethod.POST)
    public void sendNotification(@RequestBody notification notification,
                                   @RequestParam String token) throws FirebaseMessagingException {
         notificationService.sendNotification(notification, token);
    }

    @RequestMapping(value = "/send-notification-topic",method = RequestMethod.POST)
    public void sendNotificationTopic(@RequestBody notification notification,
                                 @RequestParam String topic) throws FirebaseMessagingException {
        notificationService.sendNotificationTopic(notification, topic);
    }
}
